package com.tcz.aem.icici.core.models;

import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Slf4j
public class SampleModel {

    @ValueMapValue
    @Default(values = "")
    private String text;

    @PostConstruct
    protected void init() {
        log.info("Sample Model Called!");
    }
}
